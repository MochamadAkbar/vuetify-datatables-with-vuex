import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://todo.pro/api/'

export default new Vuex.Store ({
    state: {
        options: {
            sortBy: [],
            sortDesc: [],
            page: 1,
            itemsPerPage: 5,
        },
        itemsPerPageOptions: [5, 10, 20, 30, 40, 50],
        todos: [],
        totalTodos: 0,
    },
    mutations: {
        setPagination (state, payload) {
            state.options = payload
        },
        SET_ITEMS (state, payload) {
          console.log(payload);
          state.todos = payload.items
          state.totalTodos = payload.totalItems
        }
    },
    actions: {
      queryItems (context) {
          return new Promise((resolve) => {

              const { sortBy, sortDesc, page, itemsPerPage } = context.state.options

              setTimeout(() => {
                  let _desserts = [
                      {
                        name: 'Frozen Yogurt',
                        calories: 159,
                        fat: 6.0,
                        carbs: 24,
                        protein: 4.0,
                        iron: '1%',
                      },
                      {
                        name: 'Ice cream sandwich',
                        calories: 237,
                        fat: 9.0,
                        carbs: 37,
                        protein: 4.3,
                        iron: '1%',
                      },
                      {
                        name: 'Eclair',
                        calories: 262,
                        fat: 16.0,
                        carbs: 23,
                        protein: 6.0,
                        iron: '7%',
                      },
                      {
                        name: 'Cupcake',
                        calories: 305,
                        fat: 3.7,
                        carbs: 67,
                        protein: 4.3,
                        iron: '8%',
                      },
                      {
                        name: 'Gingerbread',
                        calories: 356,
                        fat: 16.0,
                        carbs: 49,
                        protein: 3.9,
                        iron: '16%',
                      },
                      {
                        name: 'Jelly bean',
                        calories: 375,
                        fat: 0.0,
                        carbs: 94,
                        protein: 0.0,
                        iron: '0%',
                      },
                      {
                        name: 'Lollipop',
                        calories: 392,
                        fat: 0.2,
                        carbs: 98,
                        protein: 0,
                        iron: '2%',
                      },
                      {
                        name: 'Honeycomb',
                        calories: 408,
                        fat: 3.2,
                        carbs: 87,
                        protein: 6.5,
                        iron: '45%',
                      },
                      {
                        name: 'Donut',
                        calories: 452,
                        fat: 25.0,
                        carbs: 51,
                        protein: 4.9,
                        iron: '22%',
                      },
                      {
                        name: 'KitKat',
                        calories: 518,
                        fat: 26.0,
                        carbs: 65,
                        protein: 7,
                        iron: '6%',
                      },
                    ]
                  let items = _desserts.slice()
                  const totalItems = items.length
                  if (sortBy.length === 1 && sortDesc.length === 1) {
                      items = items.sort((a, b) => {
                        const sortA = a[sortBy[0]]
                        const sortB = b[sortBy[0]]

                        if (sortDesc[0]) {
                          if (sortA < sortB) return 1
                          if (sortA > sortB) return -1
                          return 0
                        } else {
                          if (sortA < sortB) return -1
                          if (sortA > sortB) return 1
                          return 0
                        }
                      })
                    }

                  if (itemsPerPage > 0) {
                  items = items.slice((page - 1) * itemsPerPage, page * itemsPerPage)
                  }
                  context.commit('_setItems', { items:items, totalItems:totalItems })

                  resolve()
              }, 1000)
          })
      },
      retrieveTodos ({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
          const { sortBy, sortDesc, page, itemsPerPage } = state.options
          let search = typeof payload === 'undefined' ? '' : (payload.search != 'null' ? payload.search : '')
          if (search == null) {
            search = ''
          }
          let startDate = typeof payload === 'undefined' ? '' : (typeof payload.startDate != 'undefined' ? payload.startDate : '')
          let endDate = typeof payload === 'undefined' ? '' : (typeof payload.endDate != 'undefined' ? payload.endDate : '')
          axios.get(`todos?page=${page}` +
                    `&itemsPerPage=${itemsPerPage}` +
                    `&sortBy=${sortBy}` +
                    `&sortDesc=${sortDesc}` +
                    `&search=${search}` +
                    `&startDate=${startDate}` +
                    `&endDate=${endDate}`
                    )
            .then((response) => {

              let items = response.data.items
              let totalItems = response.data.totalItems
              commit('SET_ITEMS', {items: items, totalItems: totalItems})
              resolve({
                items,
                totalItems
              })
            })
            .catch((error) => {
              if (error.response) {
                reject(error)
              }
            })
        })
      }
    },
    getters: {
        loading (state) {
          return state.loading
        },
        options (state) {
          console.log(state.options);
          return state.options
        },
        items (state) {
          return state.todos
        },
        totalTodos (state) {
            return state.totalTodos
        },
        itemsPerPageOptions (state) {
          return state.itemsPerPageOptions
        },
        todos (state) {
          return state.todos
        }
    }
})